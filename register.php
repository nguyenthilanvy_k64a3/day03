<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="register.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <title>Document</title>
</head>
<body>
    <div class="content mt-3">
        <form action="">
            <div class="d-flex mt-3 justify-content-center">
                <label class="me-3 label" for="username">Họ và tên</label>
                <input class="form-control input" type="text">
            </div>
            <div class="d-flex align-items-center mt-3">
                <label class="me-3 label">Giới tính</label>
                <?php
                    $gt = array("0"=>"Nam", "1"=>"Nữ");
                    for ($i = 0; $i<count($gt); $i++) {
                        echo '<div class="form-check form-check-inline">
                        <input class="form-check-input input" type="radio" name="gender" id="gender'. $i .'" value="'. $i .'">
                        <label class="form-check-label text-dark" for="gender'. $i .'">'. $gt[$i] .'</label>
                    </div>';
                    }
                    
                ?>
            </div>
            <div class="d-flex mt-3">
                <label class="me-3 label" for="password">Phân khoa</label>
                <select name="khoa" id="" class="form-select input">
                    <option value=""></option>
                    <?php
                        $khoa = array("MAT"=>"Khoa học máy tính", "KDL"=>"Khoa học vật liệu");
                        foreach ($khoa as $key => $value) {
                            echo '<option value="'.$key.'">'.$value.'</option>';
                        }

                    ?>
                </select>
            </div>
            <input class="btn mt-3 text-light" type="submit" value="Đăng ký">
        </form>
    </div>
    
    

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
</body>
</html>